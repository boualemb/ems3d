var renderer, scene, camera, control, composer, light;
var mouse2D    = new THREE.Vector2( 0, 0);
var textureSize    = new THREE.Vector2( 4096, 2048);
var params = {
    exposure: 1.5,
    bloomStrength: 0.6,
    bloomThreshold: 0,
    bloomRadius: 0
};

var GIS = {
    latitude : 0.0,
    longitude : 0.0
}
var earth, moon, sun;

const SunSize = 3;
const EarthSize = 2;
const MoonSize = 0.5;


const distanceEarthSun = 25;
const distanceEarthMoon = 5;

const sunRotationSpeed = 0.005;
const EarthRotationSpeed = 0.0;
const MoonRotationAroundEarthSpeed = 0.1;
const EarthRotationAroundSunSpeed = 0.01;

init ();
animate();

function init ()
{
    //Initialise the renderer
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    renderer.toneMapping = THREE.ReinhardToneMapping;
    renderer.toneMappingExposure = 1.0;
    renderer.toneMappingWhitePoint = 1.0;/**/

    //Setup the scene
    scene = new THREE.Scene();
    loadEnvMap();
    // create camera
    camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
    camera.position.set( 30, 10, 30 );
    // define controls
    control = new THREE.OrbitControls( camera, renderer.domElement );

    createSun();
    createEarth();
    createMoon();

    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild(renderer.domElement);
    window.addEventListener( 'resize', onWindowResize, false );
    document.addEventListener( 'mousedown', mouseClick, false );
    var renderScene = new THREE.RenderPass( scene, camera );

    var bloomPass = new THREE.UnrealBloomPass( new THREE.Vector2( window.innerWidth, window.innerHeight ), 1.5, 0.4, 0.85 );
    bloomPass.threshold = params.bloomThreshold;
    bloomPass.strength = params.bloomStrength;
    bloomPass.radius = params.bloomRadius;
  /***/
    composer = new THREE.EffectComposer( renderer );
    composer.addPass( renderScene );
    composer.addPass( bloomPass );
}
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    earth.rotation.z +=0.05; 
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}
function mouseClick(event)
{
    mouse2D.x =   ( event.clientX / window.innerWidth  ) * 2 - 1;
    mouse2D.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
    var rayCaster = new THREE.Raycaster();
    rayCaster.setFromCamera( mouse2D, camera );
    var intersectionList = rayCaster.intersectObjects( scene.children );
    if(intersectionList.length>0 && intersectionList[0].object.name == 'earth')
    {
        
        var data = intersectionList[0];
        var d = data.point.clone().normalize();
		var u = Math.round(textureSize.x * (1 - (0.5 + Math.atan2(d.z, d.x) / (2 * Math.PI))));
        var v = Math.round(textureSize.y * (0.5 - Math.asin(d.y) / Math.PI));
        const halfWidth = (textureSize.x/2);
        const halfHeight = (textureSize.y/2);
        var uv = new THREE.Vector2((u-halfWidth)/halfWidth, -(v-halfHeight)/halfHeight)
        uv2Gis(uv);
        console.log (GIS);
        
    }
   

}
function uv2Gis(uv)
{
    GIS.longitude =  uv.x * 180;
    GIS.latitude = uv.y * 90;
}

function animate(){
    requestAnimationFrame( animate );
    earth.rotation.y +=EarthRotationSpeed;
    sun.rotation.y +=sunRotationSpeed;
    rotateSun(EarthRotationAroundSunSpeed);
    rotateMoon(MoonRotationAroundEarthSpeed);
    control.update();
    composer.render( scene, camera );

}

function loadEnvMap()
{
    scene.background = new THREE.CubeTextureLoader()
	.setPath( 'maps/env/' )
	.load( [
		'px.jpg',
		'nx.jpg',
		'py.jpg',
		'ny.jpg',
		'pz.jpg',
		'nz.jpg'
	] );
}

function createSun()
{
    var sunGeom = new THREE.SphereGeometry( SunSize, 16, 16 );
    var sunMat = new THREE.MeshStandardMaterial(  );
    sunMat.emissive = new THREE.Color( 0xffff00 );
    sunMat.specular = new THREE.Color( 'black');
    sunMat.emissiveMap = new THREE.TextureLoader().load( "maps/sun/1k_sun.jpg" ); 
    sun = new THREE.Mesh(sunGeom, sunMat);
    sun.name= 'sun';
    scene.add(sun);
    sun.position.set(distanceEarthSun,0,0);
    //add light
    light = new THREE.PointLight(0xffffff);
    light.position.copy(sun.position);

    light.castShadow = true;  
    light.shadow.mapSize.width = 1024;
    light.shadow.mapSize.height = 1024;
    light.shadow.camera.near = 0.5;
    light.shadow.camera.far = 500;
    scene.add(light);
}
function createEarth()
{
    var earthGeom = new THREE.SphereGeometry( EarthSize, 32, 32 );
    var earthMat = new THREE.MeshPhongMaterial(  );

    earthMat.map = new THREE.TextureLoader().load( "maps/earth/earthmap4k.jpg" );
    earthMat.bumpMap  = new THREE.TextureLoader().load( "maps/earth/earthbump4k.jpg" );
    earthMat.bumpScale = 0.08;
    earthMat.specularMap = new THREE.TextureLoader().load( "maps/earth/earthspec4k.jpg" ); 
    earthMat.wireframe = false;
    earthMat.shininess = 100;
/*
    var earthCloudMat = new THREE.MeshPhongMaterial(  );
    earthCloudMat.map = new THREE.TextureLoader().load( "maps/earth/earthcloudmap.jpg" );
    earthCloudMat.alphaMap = new THREE.TextureLoader().load( "maps/earth/earthcloudmaptrans.jpg" );
*/
    earth = new THREE.Mesh(earthGeom, earthMat);
    earth.position.set(0,0,0);
    earth.castShadow = true;
    earth.receiveShadow = true;
    earth.name='earth';
    scene.add(earth);
}

function createMoon()
{
    var moonGeom = new THREE.SphereGeometry( MoonSize, 8, 8 );
    var moonMat = new THREE.MeshPhongMaterial(  );

    moonMat.map = new THREE.TextureLoader().load( "maps/moon/moonmap512.jpg" );
    moonMat.bumpMap  = new THREE.TextureLoader().load( "maps/moon/moonbump512.jpg" );
    moonMat.bumpScale = 0.01;
    moonMat.wireframe = false;

    moon = new THREE.Mesh(moonGeom, moonMat);
    moon.castShadow = true;
    moon.receiveShadow = true;
    moon.position.set(distanceEarthMoon,0,0);
    moon.name='moon';
    scene.add(moon);
    
}
function rotateSun(speed)
{
    var currentPosition = sun.position;
    currentPosition.sub(earth.position);
    var sunDistance = currentPosition.length();
    var direction = currentPosition.normalize();
    direction.applyAxisAngle(new THREE.Vector3(0,1,0), speed);
    direction.multiplyScalar(sunDistance);
    direction.add(earth.position);
    light.position.copy(sun.position);
}
function rotateMoon(speed)
{
    var currentPosition = moon.position;
    currentPosition.sub(earth.position);
    //var moonDistance = currentPosition.length();
    var direction = currentPosition.normalize();
    direction.applyAxisAngle(new THREE.Vector3(0,1,0), speed);
    direction.multiplyScalar(distanceEarthMoon);
    direction.add(earth.position);
}